# -*- coding: utf-8 -*-
"""
Created on Mon Apr  4 13:31:39 2022

@author: btcampb77
"""
import numpy as np
import matplotlib.pyplot as plt
from numpy import cross, dot
from numpy.linalg import norm
from math import acos, cos, sin, pi, sqrt, cosh, sinh
from numba import njit as jit

@jit
def stumpS(z):
    if z > 0:
        s = (sqrt(z) - sin(sqrt(z))) / (sqrt(z))**3
    elif z < 0:
        s = (sinh(sqrt(-z)) - sqrt(-z)) / (sqrt(-z))**3
    else:
        s = 1/6
    return s

@jit
def stumpC(z):
    if z > 0:
        c = (1 - cos(sqrt(z))) / z
    elif z < 0:
        c = (cosh(sqrt(-z)) - 1) / (-z)
    else:
        c = 1/2
    return c

@jit
def Y(z,r1,r2,A):
    Y = r1 + r2 + A*(z * stumpS(z) - 1) / sqrt(stumpC(z))
    return Y

@jit
def F(z,t,r1,r2,A,mu):
    try:
        F = ((Y(z,r1,r2,A) / stumpC(z))**(3/2)) * stumpS(z) + A*sqrt(Y(z,r1,r2,A)) - sqrt(mu)*t
        return F
    except:
        return 0
@jit
def dFdz(z,r1,r2,A):
    if z == 0:
        dF = sqrt(2)/40*Y(0,r1,r2,A)**(3/2) + A/8*(sqrt(Y(0,r1,r2,A)) + A*sqrt(1/2/Y(0,r1,r2,A)))
    else:
        dF = (Y(z,r1,r2,A)/stumpC(z))**(3/2) * (1/2/z*(stumpC(z) - 3*stumpS(z)/2/stumpC(z)) \
            + 3*stumpS(z)**2/4/stumpC(z)) + A/8*(3*stumpS(z)/stumpC(z)*sqrt(Y(z,r1,r2,A)) \
            + A*sqrt(stumpC(z)/Y(z,r1,r2,A)))
    return dF

@jit
def Lambert(R1, R2, tof, mu, prograde=True):
    
    tof = tof*24*3600 # Convert to seconds
   
    # Calc state vector magnitudes
    r1,r2 = norm(R1), norm(R2)
    # Calc change in true anomaly (dtheta) from position vector 1 to 2
    if prograde:
        if cross(R1,R2)[2] >= 0:
            dtheta = acos(dot(R1,R2) / (r1*r2))
        else:
            dtheta = 2*pi - acos(dot(R1,R2) / (r1*r2))
    else:
        if cross(R1,R2)[2] >= 0:
            dtheta = 2*pi - acos(dot(R1,R2) / (r1*r2))
        else:
            dtheta = acos(dot(R1,R2) / (r1*r2))
    
    # Calc A to eliminate angular momentum from equations
    A = sin(dtheta) * sqrt((r1*r2) / (1 - cos(dtheta)))


    # ztest = np.linspace(0,2,20)    
    # Ftest = np.zeros(len(ztest))
    
    # for i in range(len(Ftest)):
    #     Ftest[i] = F(ztest[i],tof,r1,r2,A,mu)
    # plt.plot(ztest,Ftest)
    
    # Calc root of F(z,t) with Newton's Method
    z = 1.5
    while(F(z,tof,r1,r2,A,mu) < 0):
        
        z = z + 0.1
    
    tol = 1E-9
    nmax = 5000
    
    ratio = 1
    n = 0
    while(abs(ratio) > tol) and (n <= nmax):
        n += 1
        ratio = F(z,tof,r1,r2,A,mu) / dFdz(z,r1,r2,A)
        z = z - ratio


    # Calc Lagrange Coefficients
    f = 1 - Y(z,r1,r2,A) / r1
    g = A * sqrt(Y(z,r1,r2,A) / mu)
    gdot = 1 - Y(z,r1,r2,A) / r2
    
    # Calc velocity vectors
    V1 = 1 / g * (R2 - f*R1)
    V2 = 1 / g * (gdot*R2 - R1)
    
    return V1, V2

