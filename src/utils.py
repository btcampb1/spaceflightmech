# -*- coding: utf-8 -*-
"""
Created on Mon Apr  4 12:50:42 2022

@author: btcampb77
"""

from numpy.linalg import norm
from datetime import datetime, timedelta
from orbit import R_V, orb_E
from bodies import Sun
from math import floor, sqrt, sin, pi, tan, atan
from scipy.optimize import bisect
from lambert import Lambert
from numpy import zeros, meshgrid, linspace
import matplotlib.pyplot as plt
        

class Time():
    def datetime_range(start, end, res='mid'):
        # Date in iso format "yyyy-mm-dd"
        d1 = datetime.fromisoformat(start) 
        d2 = datetime.fromisoformat(end)
        
        steps = (d2-d1).days
        
        if res=="high":
            window = [d1 + timedelta(days=0.5*x) for x in range(2*steps+1)]
        if res=="mid":
            window = [d1 + timedelta(days=x) for x in range(steps+1)]
        if res=="low":
            window = [d1 + timedelta(days=5*x) for x in range(int(0.2*steps+1))]
        if res=="scan":
            window = [d1 + timedelta(days=30*x) for x in range(int(steps/30+1))]
       
        # for date in window:
        #     print(date)
        
        return window
    
    def tof_list(launch_window, arrival_window):
        # Date in iso format "yyyy-mm-dd"
        if min(arrival_window) > min(launch_window):
            min_tof = min(arrival_window) - max(launch_window)
        else:
            min_tof = timedelta(0)
        # print(min_tof)
        max_tof = max(arrival_window) - min(launch_window)
        # print(max_tof)
        steps = 2*(max_tof-min_tof).days
        tof_range = [min_tof + timedelta(days=0.5*x) for x in range(steps+1)]
        # print(tof_range)
        return tof_range

    def calc_tof(depart, arrive):
        d1J = State.julian(depart)
        d2J = State.julian(arrive)
        tof = d2J - d1J
        return tof
        
     
class State():
    def julian(datetime):
        date = datetime.date()
        time = datetime.time()
        y = date.year
        M = date.month
        d = date.day
        h = time.hour
        m = time.minute
        s = time.second
        J0 = 367*y - int((7*(y+int((M+9)/12)))/4) + int((275*M)/9) + d + 1721013.5
        UT = h + m/60 + s/3600
        JD = J0 + UT/24
        return JD
    
    def stateVec(JD, body, primary):
        Au = 1.49597871E8 #km 
        T0 = (JD - 2451545.0) / 36525.
        a = (body.a0 + body.adot*T0)*Au
        e = body.e0 + body.edot*T0
        i = body.i0 + body.idot*T0
        RAAN = body.RAAN0 + body.RAANdot*T0
        p_long = (body.p_long0 + body.p_longdot*T0) - 360*(floor((body.p_long0 + body.p_longdot*T0)/360))
        L = body.L0 + body.Ldot*T0 - 360*floor((body.L0 + body.Ldot*T0)/360)
        h = sqrt(primary.mu*a*(1-e**2))
        arg_p = p_long - RAAN
        M = L - p_long
        fE = lambda E,e,M : E - e*sin(E) - M
        E = (bisect(fE,-2*pi, 2*pi,args=(e,M*pi/180)))*180/pi
        E -= 360*floor((E/360))
        theta = (2*atan(sqrt((1+e)/(1-e))*tan(E*(pi/180)/2)))*180/pi
        theta -= 360*floor((theta)/360)
        R,V = R_V(h,e,i,RAAN,arg_p,theta,primary.mu)
        return R,V
        

class Transfer():
    def interplanetary(depart, arrive, planet1, planet2, primary, dep_alt=180, cap_altP=300, cap_T=48):
        # Calc dates/times
        # depart = datetime.fromisoformat(depart)
        # arrive = datetime.fromisoformat(arrive)
        tof = Time.calc_tof(depart,arrive)
        dJ = State.julian(depart)
        aJ = State.julian(arrive)
        
        # Calc departure orbit radius and capture orbit altitude periapsis and period
        r_dep = planet1.R + dep_alt
        r_capP = planet2.R + cap_altP
        
        # Calc state vectors of planets
        R1, V1 = State.stateVec(dJ,planet1,primary)
        R2, V2 = State.stateVec(aJ,planet2,primary)
        r1, v1 = norm(R1), norm(V1)
        r2, v2 = norm(R2), norm(V2)
        
        # Calc spacecraft departure and arrival velocities (from and to sphere of influence)
        try: VD, VA = Lambert(R1, R2, tof, primary.mu, True)
        except: VD, VA = 100, 100
        # Calc transfer orbit elements and hyperbolic excess departure and arrival velocities
        # tOrb_E = orb_E(R1,VD,Sun.mu)    #[h,e,RAAN,i,arg_p,theta,a]
        VD_inf = VD - V1 
        VA_inf = VA - V2
        vd_inf, va_inf = norm(VD_inf), norm(VA_inf)
        
        # Calc deltaV and C3 of departure from circular parking orbit
        vdp = sqrt(vd_inf**2 + (2*planet1.mu/r_dep))
        v_park = sqrt(planet1.mu/r_dep)
        deltaV_D = vdp - v_park
        C3d = vd_inf**2
        
        # Calc deltaV and C3 of arrival to elliptical capture orbit
        vap = sqrt(va_inf**2 + (2*planet2.mu/r_capP))
        a_cap = ((cap_T*3600*sqrt(planet2.mu))/ (2*pi))**(2/3)
        e_cap = 1 - r_capP/a_cap
        v_cap = sqrt(planet2.mu*(1+e_cap)/r_capP)
        deltaV_C = vap - v_cap
        e_appr = 1 + r_capP*va_inf**2/planet2.mu
        C3a = va_inf**2
        
        # Calc C3
        C3 = abs(C3d) + abs(C3a)
        
        # Calc total deltaV
        DELTAV = abs(deltaV_D)+abs(deltaV_C)
        
        transfer = [DELTAV, C3, tof]
        
        return transfer
        
    def PorkChop(departure_window, arrival_window, Planet1, Planet2):

        launches = len(departure_window)
        arrivals = len(arrival_window)        

        dv = zeros((launches,arrivals))
        C3 = zeros((launches,arrivals))
        dur = zeros((launches,arrivals))

        for i, depart in enumerate(departure_window):
            for j, arrive in enumerate(arrival_window):
                dv[i,j], C3[i,j], dur[i,j] = Transfer.interplanetary(depart, arrive, Planet1, Planet2, Sun)

        X, Y = meshgrid(departure_window, arrival_window)
        fig, ax = plt.subplots()
        fig.autofmt_xdate()
        # cp1 = ax.contourf(X, Y, C3.transpose(),(0,2,4,6,8,10,12.5,15,17.5,20,22.5,25,27.5,30,35,40,45),cmap='viridis', alpha=1)
        cp1 = ax.contourf(X, Y, C3.transpose(),[0,3,6,9,12,15,18,21,24,27,30,33,36,38,40,45,50,55,60,70,80], cmap='viridis', alpha=1)
        cbar = fig.colorbar(cp1) # Add a colorbar to a plot
        cbar.set_label("km2/s2")
        cp2 = ax.contour(X,Y, dv.transpose(), [0,1,2,3,4,5,6,7,8,9,10], linestyles="dashed", colors='black')
        plt.clabel(cp2, inline=True, fontsize=8)
        ax.contour(X, Y, C3.transpose(), [0,3,6,9,12,15,18,21,24,27,30,33,36,38,40,45,50,55,60,70,80], alpha=0.15, colors='black')
        
        ax.set_title(str(Planet1.name+" - "+Planet2.name+", dV & C3 Launch Windows"))
        ax.set_xlabel("Departure Date")
        ax.set_ylabel("Arrival Date")
        
        plt.show()
        
            
            
# [0,3,6,9,12,15,18,21,24,27,30,33,36,38,40,45,50,55,60,70,80]
# [0,1,2,3,4,5,6,7,8,9,10]