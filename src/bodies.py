# -*- coding: utf-8 -*-
"""
Created on Fri Apr  8 18:27:41 2022

@author: btcampb77
"""
from datetime import timedelta

class Body():
    def __init__(self, name, parent, mu, R, rotational_period, J2, mass, a0, adot, e0, edot, i0, idot, RAAN0, RAANdot, p_long0, p_longdot, L0, Ldot):
        self.name = name
        self.parent = parent
        self.mu = mu
        self.R = R
        self.rotational_period = rotational_period
        self.J2 = J2
        self.mass = mass
        self.a0 = a0
        self.adot = adot
        self.e0 = e0
        self.edot = edot
        self.i0 = i0
        self.idot = idot
        self.RAAN0 = RAAN0
        self.RAANdot = RAANdot
        self.p_long0 = p_long0
        self.p_longdot = p_longdot
        self.L0 = L0
        self.Ldot = Ldot


Sun = Body("Sun",
            None,
            1.327124E11,
            6.957E8,
            None,
            0.00108263,
            1.9891E30,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None
            )

Earth = Body("Earth",
             Sun,
             398600,
             6378,
             timedelta(days=365, hours=6, minutes=9),
             J2 = 0.0010826,
             mass = 5.972E24,
             a0 = 1.00000261, #Au
             adot = 0.00000562, #Au/Cy
             e0 = 0.01671123,
             edot = -0.00004392,
             i0 = -0.00001531,
             idot = -0.01294668,
             RAAN0 = 0,
             RAANdot = 0.0,
             p_long0 = 102.93768193,
             p_longdot = 0.32327364,
             L0 = 100.46457166,
             Ldot = 35999.37244981
             )

Mars = Body("Mars",
            Sun,
            42830,
            3380,
            timedelta(days=365*1.881),
            None,
            None,
            a0 = 1.52371034, #Au
            adot = 0.0001847, #Au/Cy
            e0 = 0.09339410,
            edot = 0.00007882,
            i0 = 1.84969142,
            idot = -0.008131131,
            RAAN0 = 49.55953891,
            RAANdot = -0.29257343,
            p_long0 = -23.94362959,
            p_longdot = 0.44441088,
            L0 = -4.55343205,
            Ldot = 19140.30268499
            )

Venus = Body("Venus",
            Sun,
            324900,
            6052,
            timedelta(days=224.7),
            None,
            None,
            a0 = 0.72333566, #Au
            adot = 0.00000390, #Au/Cy
            e0 = 0.00677672,
            edot = -0.00004107,
            i0 = 3.39467605,
            idot = -0.00078890,
            RAAN0 = 76.67984255,
            RAANdot = -0.2776918,
            p_long0 = 102.93768193,
            p_longdot = 0.00268329,
            L0 = 181.97909950,
            Ldot = 58517.81538729
             )

Pluto = Body("Pluto",
             Sun,
             830,
             1195,
             timedelta(days=247.7*365),
             None,
             None,
             a0 = 39.48211675,
             adot = -0.00031695,
             e0 = 0.24882730,
             edot = 0.00005170,
             i0 = 17.14001206,
             idot = 0.00004818,
             RAAN0 = 110.30393684,
             RAANdot = -0.01183482,
             p_long0 = 224.06891629,
             p_longdot = -0.04062942,
             L0 = 238.92903833,
             Ldot = 145.20780515
             )

















