# -*- coding: utf-8 -*-
"""
Created on Mon Apr  4 23:29:55 2022

@author: btcampb77
"""

from numpy import cross, dot, arccos, pi, array, sin, cos, matmul
from numpy.linalg import norm


def orb_E(R1,V1,mu):
    degrees = 180/pi
    r1 = norm(R1)
    # v1 = norm(V1)
    
    v1_r = dot(R1,V1)/r1

    H = cross(R1, V1.T)
    h = norm(H)

    i = arccos(H[2]/h)*degrees
    
    N = cross([0,0,1],H)
    n = norm(N)

    if N[1] >= 0:
        RAAN = arccos(N[0]/n)*degrees
    elif N[1] < 0:
        RAAN = 360 - arccos(N[0]/n)*degrees

    E = cross(V1,H)/mu - R1/r1
    e = norm(E)

    if E[2] >= 0:
        arg_p = arccos(dot(N,E)/(n*e))*degrees
    elif E[2] < 0:
        arg_p = 360 - arccos(dot(N,E)/(n*e))*degrees

    if v1_r >= 0:
        theta = arccos(dot(E,R1)/(e*r1))*degrees
    elif v1_r < 0:
        theta = 360 - arccos(dot(E,R1)/(e*r1))*degrees
    
    a = h**2/mu * (1/(1-e**2))
    
    # print('\n ORBITAL ELEMENTS \n')
    # print("1. Spec Ang Momentum = ", round(h,5), 'km^2/s')
    # print('2. Eccentricity = ', round(e,5))
    # print('3. RAAN = ', round(RAAN,5), 'deg')
    # print('4. Inclination = ', round(i,5), 'deg')
    # print('5. Argument of Perigee = ', round(arg_p,5), 'deg')
    # print('6. True Anomaly = ', round(theta,5), 'deg')
    # print('7. Semi-major Axis = ', round(a,5), 'km\n')
    
    return [h, e, RAAN, i, arg_p, theta, a]


def R_V(h,e,i,RAAN,arg_p,theta, mu):
    sind = lambda degrees: sin(degrees*pi/180)
    cosd = lambda degrees: cos(degrees*pi/180)
    
    Q = array([[-sind(RAAN)*cosd(i)*sind(arg_p)+cosd(RAAN)*cosd(arg_p), -sind(RAAN)*cosd(i)*cosd(arg_p)-cosd(RAAN)*sind(arg_p), sind(RAAN)*sind(i)],
               [cosd(RAAN)*cosd(i)*sind(arg_p)+sind(RAAN)*cosd(arg_p), cosd(RAAN)*cosd(i)*cosd(arg_p)-sind(RAAN)*sind(arg_p), -cosd(RAAN)*sind(i)],
               [sind(i)*sind(arg_p), sind(i)*cosd(arg_p), cosd(i)]
               ])
    R_PF = h**2/mu * 1/(1+e*cosd(theta)) * array([cosd(theta),sind(theta),0])
    V_PF = mu/h * array([-sind(theta), e+cosd(theta), 0])
    R = matmul(Q,R_PF)
    V = matmul(Q,V_PF)
    return R, V
