# -*- coding: utf-8 -*-
"""
Created on Mon Apr  4 23:16:32 2022

@author: btcampb77
"""

from utils import Time, Transfer
from bodies import Sun, Earth, Venus, Mars, Pluto
import time

def main():

    tic = time.perf_counter()
    departure_window = Time.datetime_range("2007-03-01", "2007-11-01", res="mid")
    arrival_window = Time.datetime_range("2008-02-01", "2008-06-01", res="mid")

    Transfer.PorkChop(departure_window, arrival_window, Mars, Earth)
    toc = time.perf_counter()
    print(toc-tic, " seconds")

if __name__=='__main__':
    main()